package com.example.almaauliayasmin_10191006.basicandroid;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    //Deklarasi variable pendukung
    private TextView Hasil;
    private EditText Masukkan;
    private Button Eksekusi;

    //Deklarasi dan inisialisasi SharedPreferences
    private SharedPreferences preferences;

    //Digunakan untuk konfigurasi SharedPreferences
    private SharedPreferences.Editor editor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Masukkan = findViewById(R.id.input);
        Hasil = findViewById(R.id.output);
        Eksekusi = findViewById(R.id.save);

        //Membuat file baru beserta modifiernya
        preferences = getSharedPreferences("Belajar_SharedPreferences", Context.MODE_PRIVATE);

        Eksekusi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
                Toast.makeText(getApplicationContext(),"Data Tersimpan",Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getData() {
        //Mendapatkan input dari user
        String getContent = Masukkan.getText().toString();
        //Digunakan untuk pengaturan konfigurasi SharedPreferences
        editor = preferences.edit();
        //Memasukkan data pada editor SharedPreferences dengan key (data_saya)
        editor.putString("data_saya", getContent);
        //Menjalankan operasi
        editor.apply();
        //Menampilkan output
        Hasil.setText("Output Data : "+preferences.getString("data_saya", null));

    }
}